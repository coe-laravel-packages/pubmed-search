<?php

namespace Coe\PubMedSearch;

use Illuminate\Support\Facades\Log;

// EUtils
class PubMedUtils
{
	// -- 
	public $url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/{action}.fcgi';
	public $db  = 'pubmed';

	// --
	public function query($action, $query, $method = 'GET', $returnObject = true)
	{
		$body = '';

		// Setup URL
		$URL  = str_replace('{action}', $action, $this->url);
		if($method == 'GET')
			$URL .= '?' . http_build_query($query);
		else if($method == 'POST')
			$query['db'] = $this->db;

		// cURL
		$cURL = curl_init($URL);
                if ($method == 'POST') {
                    curl_setopt($cURL, CURLOPT_POST, 1);
                    curl_setopt($cURL, CURLOPT_POSTFIELDS, $query);
                }
		curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($cURL);
		curl_close($cURL);

		if($returnObject) {
			try {
				return simplexml_load_string($result);
			} catch (\Exception $e) {
				Log::error("Exception parsing pubmed result. Exception follows:");
				Log::error(print_r($e->getTrace(), TRUE));
				Log::error("cURL Result Object:");
				Log::error(print_r($result, TRUE));
			}
		}

		return $result;
	}
}
