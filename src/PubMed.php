<?php

// -----------------------------------------------------------------------------
// -- Very Simple PHP Library for EUtils/PubMed
// -- https://www.github.com/robertaboukhalil/pubmed
// -----------------------------------------------------------------------------
namespace Coe\PubMedSearch;

// PubMed
class PubMed extends PubMedUtils
{
	// -- 
	public function PubMed()
	{
		$this->db = 'pubmed';
	}

	// -- 
	public function esearch($query, $returnObject = true)
	{
		return $this->query('esearch', $query, 'GET', $returnObject);
	}

	// -- 
	public function efetch($query, $returnObject = true)
	{
		return $this->query('efetch', $query, 'POST', $returnObject);
	}
}
