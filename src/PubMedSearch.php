<?php

// -----------------------------------------------------------------------------
// -- Very Simple PHP Library for EUtils/PubMed
// -- https://www.github.com/robertaboukhalil/pubmed
// -----------------------------------------------------------------------------
namespace Coe\PubMedSearch;

class PubMedSearch {

    public function __construct($email=null, $apikey=null) {
        $this->email = $email;
        $this->apikey = $apikey;
        if (!$email) {
            $this->email = config('publications.pubmed_api_email');
        }
        if (!$apikey) {
            $this->apikey = config('publications.pubmed_api_key');
        }
    }
    
    public function search($author, $fromDate = "1900/01/01") {
        $apiResponse = $this->searchByAuthorAndDate($author, $fromDate);
        return $apiResponse['publications'];
    }
    
    public function searchByAuthorAndDate($author, $fromDate = "1900/01/01", 
                                       $retmax=100, $retstart=0, $db='pubmed') : SearchResults {

        //reformat date if it's the hyphenated one
        if (preg_match('/^(\d\d\d\d)-(\d\d)-(\d\d)$/', $fromDate, $matches)) {
            $fromDate = str_replace('-','/', $fromDate);
        }

        $authorLimit = "($author" . "[author])";
        $dateLimit = ' AND ("' . $fromDate . '"[Date - Publication] : "3000"[Date - Publication])';
        $query = $authorLimit . $dateLimit;
        
        $apiResponse = $this->searchGeneric($query, $retmax, $retstart, $db);

        //TODO: find a more elegant way of handling pagination and order with large results
        //get the last (oldest) 100 results if more than 100 returned
        if ($apiResponse->count > $retmax && $retstart == 0 ) {
            $retstart = $apiResponse->count - $retmax;
            $apiResponse = $this->searchGeneric($query, $retmax, $retstart, $db);
        }

        return $apiResponse;
    }
    
    public function searchGeneric($term, $retmax=100, $retstart=0, $db='pubmed') : SearchResults {
        $query = new PubMed();
        $result = $query->esearch( Array( 'term'     => $term,
                                          'retmax'   => $retmax,          // Max fields to return is 100
                                          'email'    => $this->email,
                                          'api_key'  => $this->apikey,
                                          'db'       => $db,
                                          'sort'     => 'pub date',
                                          'retstart' => $retstart)            // Return first results 
                                );
        
        // Output is IDs of papers
        $paperIDs = (array) $result->IdList->Id;
        $count    = (int) $result->Count;
        
        // Prepare next query to fetch abstract (first, make list of the 100 paper IDs)
        $strPaperIDs = implode(",",$paperIDs);
        
        $publications = $this->searchByIds($strPaperIDs);
        return SearchResults::fromArray(compact('paperIDs', 'count', 'retstart', 'retmax', 'publications'));
    }
    
    public function searchByIds($ids) {
        $query = new PubMed();
        
        // Query PubMed for abstracts (and paper title/author list/etc)
        // Returns XML file (set last argument to true to return SimpleXML object)
        $articleAbstracts = $query->efetch(Array( 'id'      => $ids,
                                                   'retmode' => 'xml',
                                                   'email'    => $this->email,
                                                   'api_key'  => $this->apikey,
                                                   'rettype' => 'abstract'
                                                  ));
        $articles = $this->parseAbstracts($articleAbstracts);
        return $articles;
    }
    
    public function parseAbstracts($articleAbstracts) {
        $abstracts = $articleAbstracts->PubmedArticle;
        $articles = [];
        foreach($abstracts as $abstract) {
            $articles[] = $this->parseArticle($abstract->MedlineCitation);
        }
        return $articles;
    }
    
    public function parseArticle($citation) {
        $PubMedId = (string)$citation->PMID;
        $article = $citation->Article;
        
        //author attribution
        $authors = $article->AuthorList->Author ?? [];
        $AuthorsString = [];
        for($i = 0; $i < count($authors); $i++) {
            $authname = $authors[$i];
            $AuthorsString[] = ((string)$authname->LastName) . " " . ((string)$authname->Initials);
        }
        $AuthorsString = implode(", ", $AuthorsString);
        
        //Title
        $Title = (string)$article->ArticleTitle;
        
        //source
        $Source = $article->Journal->Title;
        $Source = (array)($Source);
        $Source = $Source[0] ?? $Source;
        
        //Journal
        $PubDate = (string)$article->Journal->JournalIssue->PubDate->Year;
        $Volume  = (string)$article->Journal->JournalIssue->Volume;
        $Issue   = (string)$article->Journal->JournalIssue->Issue;
        $Pages   = (string)$article->Pagination->MedlinePgn;
        
        //SO
        $SO = sprintf("%s ; %s %s : %s",
                    $PubDate,
                    $Volume, 
                    $Issue,
                    $Pages                
                );
        
        //Journal
        $JournalReference = "$Source $SO";
        
        //link
        $Link = 'http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids=' . $PubMedId;
       
        //combine
        $result = compact(
                'PubMedId',
                'AuthorsString',
                'Source',
                'SO',
                'PubDate',
                'Volume',
                'Issue',
                'Pages',
                'Title',
                'Link',
                'JournalReference');
        return $result;
    }
}

