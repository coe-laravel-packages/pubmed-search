<?php

namespace Coe\PubMedSearch;

class SearchResults
{
    public $paperIDs = [];
    public $count = 0;
    public $retstart = 0;
    public $retmax = 0;
    public $publications = [];
    const FIELDS = ['paperIDs', 'count', 'retstart', 'retmax', 'publications'];

    public function toArray() {
        return (array)$this;
    }

    public static function fromArray($array) : SearchResults {
        $result = new SearchResults();
        foreach(self::FIELDS as $field) {
            if (isset($array[$field])) {
                $result->$field = $array[$field];
            }
        }
        return $result;
    }

    public static function emptyResult()  : SearchResults {
        return new SearchResults();
    }
}